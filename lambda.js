import serverlessExpress from '@vendia/serverless-express';
import app from './package/post/loaders/lambdaApp';

export const handler = serverlessExpress({ app });