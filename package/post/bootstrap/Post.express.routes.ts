/**
 * Routes to Post Service.
 */
import express from "express";
import upload from "./multer";

import {
  getAllPostsController,
  createPostController,
  getPostByIdController,
  updatePostController,
  destroyPostController,
  deletePostController,
  postsHealthController,
  postImageController,
  uploadImageController,
  getPostBySlugController,
  getImageFromDBController,
  uploadImageToDbController,
  updateImageController,
  getAllTagsController,
  createTagController,
  getPostsPageController,
  getPostsCountController,
} from "./bootstrap";

var router = express.Router();

router.get(
  "/api/posts/health",
  postsHealthController.handle.bind(postsHealthController)
);
router.get(
  "/api/posts/index",
  getAllPostsController.handle.bind(getAllPostsController)
);
router.get(
  "/api/posts/index/:pagesize/:page",
  getPostsPageController.handle.bind(getPostsPageController)
);
router.get(
  "/api/posts/tags/index",
  getAllTagsController.handle.bind(getAllTagsController)
);
router.get(
  "/api/posts/getpostbyid/:_id",
  getPostByIdController.handle.bind(getPostByIdController)
);
router.get(
  "/api/posts/details/:slug",
  getPostBySlugController.handle.bind(getPostBySlugController)
);
router.post(
  "/api/posts/save",
  upload.single("image"),
  createPostController.handle.bind(createPostController)
);
router.post(
  "/api/posts/tags/save",
  createTagController.handle.bind(createTagController)
);
//Funcion que actualiza el post
router.patch(
  "/api/posts/update/:_id",
  updatePostController.handle.bind(updatePostController)
);
router.patch(
  "/api/posts/delete/:_id",
  deletePostController.handle.bind(deletePostController)
);
router.delete(
  "/api/posts/destroy/:_id",
  destroyPostController.handle.bind(destroyPostController)
);
router.get(
  "/api/posts/image",
  postImageController.handle.bind(postImageController)
);
/**
 * Estas funciones son las que cargan la imagen
 */
router.post(
  "/api/posts/upload",
  upload.single("image"),
  uploadImageController.handle.bind(uploadImageController)
);
router.post(
  "/api/posts/uploadtodb",
  upload.single("image"),
  uploadImageController.handle.bind(uploadImageController)
);
/**
 * Actualiza la informacion de la base de datos cuando ya esta la imagen en el path
 */
router.patch(
  "/api/posts/updateimage",
  upload.single("image"),
  updateImageController.handle.bind(updateImageController)
);
router.get(
  "/api/posts/imagefromdb/:name/:slug",
  getImageFromDBController.handle.bind(getImageFromDBController)
);
router.get("/api/posts/count", getPostsCountController.handle.bind(getPostsCountController));

export default router;
