export interface IPostImage {
  _id: String;
  imageName: String;
  imageUrl: String;
  file: {
    data: any;
    contentType: String;
  };
  owner: String;
}
