import Command from "../command";
import { PostRepository } from "../../domain/services/Post.service.repository";

export class PostsCountGetter extends Command {
  private _repository: PostRepository;

  constructor(_repository: PostRepository) {
    super();
    this._repository = _repository;
  }

  //  Override Method
  async execute() {
    const response = await this._repository.getPostsCount();
    return response;
  }
}
