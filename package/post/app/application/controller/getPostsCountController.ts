import e from "express";
import { PostsCountGetter } from "../useCases/PostsCountGetter";

interface UseCase {
  getPostsCount: PostsCountGetter;
}

export class GetPostsCountController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const count = await this.useCase.getPostsCount.execute();
      return res.status(200).end(count.toString());
    } catch (err) {
      console.error(err);
      return res.status(400).send(err);
    }
  }
}
