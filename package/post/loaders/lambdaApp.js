import expressLoader from "./express";
import mongooseLoader from "./mongoose";
import logger from "./logger";
import config from "../config";
const express = require("express");

mongooseLoader();

export const app = express();
//Adds Express Static Middleware
app.use(express.static("public"));
expressLoader({ app });

 app.listen(config.port || 9000, () => {
    try {
      logger.info(`
        ################################################
        🛡️  Server listening on port: ${config.port} 🛡️ 
        ################################################
      `);
    } catch (err) {
      logger.error(err);
      process.exit(1);
    }
  });

export default app;

