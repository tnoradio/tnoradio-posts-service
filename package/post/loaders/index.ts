import expressLoader from "./express";
import mongooseLoader from "./mongoose";
import logger from "./logger";
import config from "../config";
const express = require("express");

export const app = express();
//Adds Express Static Middleware
app.use(express.static("public"));

export default async () => {
  /**
   * Port loader
   */
  await app.listen(config.port || 9000, () => {
    try {
      /**
       * Consul Register
       */

      //consul.register;
      logger.info(`
        ################################################
        🛡️  Server listening on port: ${config.port} 🛡️ 
        ################################################
      `);
    } catch (err) {
      logger.error(err);
      process.exit(1);
    }
  });

  /**
   * MongoDB loader, creates mongoClient and connect to the db and return db connection.
   */
  await mongooseLoader();
  logger.info("✌️ DB loaded and connected!");

  /**
   * Laods express essentials
   */
  await expressLoader({ app });
  logger.log("info", "Express Loader has initalized successfully! ✅");
};
